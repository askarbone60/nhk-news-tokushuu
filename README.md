# ＮＨＫニュース・特集

Scraping the news to make an ebook.

[ＮＨＫニュース・特集.azw3](ＮＨＫニュース・特集.azw3) (285 MB)

[ＮＨＫニュース・特集.min.azw3](ＮＨＫニュース・特集.min.azw3) (66 MB)

![kindle](images/kindle.jpeg)

```sh
$ npm start -- get-links
$ npm start -- get-pages
$ npm start -- map-to-articles
$ npm start -- download-images
$ npm start -- build-book
# Compress and resize images.
$ pandoc nhk.html -o nhk.epub
# Then open in calibre for postprocessing and conversion to KF8/AWZ3.
```
