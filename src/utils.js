import fetch from "node-fetch";
import fs from "fs";
import moment from "moment-timezone";
import puppeteer from "puppeteer";
import crypto from "crypto";
import stream from "stream";
import util from "util";

const fsp = fs.promises;
const streamPipeline = util.promisify(stream.pipeline);

export function toJson(object) {
    return JSON.stringify(object, null, 4);
}

export function fromJson(string) {
    return JSON.parse(string);
}

export async function writeUtf8(path, string) {
    fsp.writeFile(path, string, "utf-8");
}

export async function readUtf8(path) {
    return await fsp.readFile(path, "utf-8");
}

export function readUtf8Sync(path) {
    return fs.readFileSync(path, "utf-8");
}

export function sha256sum(text) {
    return crypto.createHash("sha256")
        .update(text)
        .digest("hex");
}

export async function download(url, path) {
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error(`Unexpected response: ${response.statusText}`);
    }

    return streamPipeline(response.body, fs.createWriteStream(path));
}

export async function mkdir(path) {
    await fsp.mkdir(path, { recursive: true });
}

export function formatTimestamp(timestamp) {
    return moment(timestamp).tz("Asia/Tokyo")
        .format("YYYY年M月D日 H時mm分");
}

export async function startBrowser() {
    const browser = await puppeteer.launch({
        args: [
            "--headless",
            "--no-sandbox",
            "--disable-gpu",
            "--disable-dev-shm-usage",
        ],
    });

    return browser;
}

export function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}
